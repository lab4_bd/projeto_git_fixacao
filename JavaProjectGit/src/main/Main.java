package main;

import classes.*;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int op;
		double valor;
		
		System.out.println("Digite uma op��o:\n"
				+ "1- Real -> D�lar\n"
				+ "2- D�lar -> Real\n"
				+ "3- Celsius -> Farenheit\n"
				+ "4- Farenheit -> Celsius\n"
				+ "5- Kilometros/Hora -> Metros/Segundo\n"
				+ "6- Metros/Segundo -> Kilometros/Hora");
		op = entrada.nextInt();
		System.out.println("Insira o valor a ser convertido: ");
		valor = entrada.nextDouble();
		
		switch(op){
		case 1:
			new Moeda().getDolar(valor);
			break;
		case 2:
			new Moeda().getReal(valor);
			break;
		case 3:
			new Temperatura().getFarenheit(valor);
			break;
		case 4:
			new Temperatura().getCelsius(valor);
			break;
		case 5:
			new Velocidade().getMs(valor);
			break;
		case 6:
			new Velocidade().getKmH(valor);
			break;
		default:
			System.out.println("Op��o inv�lida");
		}
		System.exit(0);
	}
}
