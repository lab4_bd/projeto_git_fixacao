package classes;

public class Moeda {
	
	public void getReal(double valor){
		System.out.println(valor / 3.16920044d + " Reais");
	}
	
	public void getDolar(double valor){
		System.out.println(valor * 3.16920044d + " Dolares");
	}
}
