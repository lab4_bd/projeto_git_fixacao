package classes;

public class Temperatura {

	public void getCelsius(double valor){
		System.out.println(((valor - 32d)/1.8d) + " Celsius");
	}
	
	public void getFarenheit(double valor){
		System.out.println((valor*1.8d + 32d) + " Farenheit");
	}
}
