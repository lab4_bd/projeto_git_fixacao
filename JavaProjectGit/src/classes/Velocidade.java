package classes;

public class Velocidade {
	
	public void getKmH(double valor){
		System.out.println( valor * 3.6d + " Kilometros por Hora" );
	}
	
	public void getMs(double valor){
		System.out.println( valor / 3.6d + " Metros por Segundo");
	}
}